//Validation
function Guid(value) {
	if (value == null)
		this.Value = Guid.prototype.Empty;
	else {
		var parsed = [];
		var val = value.toLowerCase();
		for (var i = 0; i < val.length; i++) {
			if ((val[i] >= '0' && val[i] <= '9') || (val[i] >= 'a' && val[i] <= 'f')) {
				parsed.push(val[i]);
				if (parsed.length === 36)
					break;
				if (parsed.length === 8 || parsed.length === 13 || parsed.length === 18 || parsed.length === 23)
					parsed.push('-');
			}
		}
		if (parsed.length !== 36)
			throw "Error: Invalid input for new System.Guid(): " + value;
		this.Value = parsed.join("");
	}
}

//bool, generation
function getGUID(excludeDashes) {
	var s = [];
	var hexDigits = "0123456789ABCDEF";
	for (var i = 0; i < 32; i++) {
		s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
	}
	s[12] = "4";
	s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);
	var uuid = s.join("");
	var formatted;
	if (excludeDashes)
		formatted = uuid.substr(0, 8) + uuid.substr(8, 4) + uuid.substr(12, 4) + uuid.substr(16, 4) + uuid.substr(20, 12);
	else
		formatted = uuid.substr(0, 8) + "-" + uuid.substr(8, 4) + "-" + uuid.substr(12, 4) + "-" + uuid.substr(16, 4) + "-" + uuid.substr(20, 12);
	return new Guid(formatted);
}