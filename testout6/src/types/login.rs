use std::collections::HashMap;
use uuid::Uuid;

/// Login Request Json
#[derive(Debug, serde::Serialize)]
pub struct LoginRequestJson<'a, 'b> {
    /// The Username
    pub login: &'a str,

    /// The SHA256 of the password
    #[serde(rename = "pHash")]
    pub password_hash: String,

    /// ?
    #[serde(rename = "trustDevice")]
    pub trust_device: bool,

    /// ?
    #[serde(rename = "twoFactorCode")]
    pub two_factor_code: &'b str,
}

/// Login response
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct LoginResponse {
    /// ?
    #[serde(rename = "authenticatedUser")]
    pub authenticated_user: Option<AuthenticatedUser>,

    /// ?
    #[serde(rename = "blockAutomation")]
    pub block_automation: Option<bool>,

    /// ?
    pub success: bool,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// Authenticated User field
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct AuthenticatedUser {
    /// ?
    #[serde(rename = "interAppLinks")]
    pub inter_app_links: Vec<serde_json::Value>,

    /// The associated email
    #[serde(rename = "signOnEmail")]
    pub sign_on_email: String,

    /// ?
    #[serde(rename = "signOnMobileCountryCode")]
    pub sign_on_mobile_country_code: Option<serde_json::Value>,

    /// ?
    #[serde(rename = "signOnMobileNumber")]
    pub sign_on_mobile_number: Option<serde_json::Value>,

    /// ?
    #[serde(rename = "signOnSystemId")]
    pub sign_on_system_id: i32,

    /// username?
    #[serde(rename = "signOnUserKey")]
    pub sign_on_user_key: String,

    /// ?
    #[serde(rename = "subscriptionStatus")]
    pub subscription_status: i32,

    /// ?
    #[serde(rename = "uiOptions")]
    pub ui_options: String,

    /// ?
    #[serde(rename = "userProfile")]
    pub user_profile: UserProfile,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// User Profile field
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct UserProfile {
    /// ?
    #[serde(rename = "campusUserLinks")]
    pub campus_user_links: Vec<serde_json::Value>,

    /// Given name
    #[serde(rename = "givenName")]
    pub given_name: String,

    /// ?
    #[serde(rename = "isDeleted")]
    pub is_deleted: bool,

    /// ?
    #[serde(rename = "isViewAsUser")]
    pub is_view_as_user: bool,

    /// ?
    pub merged: bool,

    // registrationDateUtc: "2019-09-04T17:06:32.957Z"
    // resultStorage: "labsimsaas_results2"
    // signOnUserProfileLink: Vec<serde_json::Value>,
    pub surname: String,

    #[serde(rename = "userProfileId")]
    pub user_profile_id: Uuid,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}
