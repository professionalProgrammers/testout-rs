/// Outline root node
#[derive(Debug, serde::Deserialize)]
pub struct Navigation {
    /// The outline course title
    #[serde(rename = "productTitle")]
    pub product_title: String,

    /// Delivery medium
    pub delivery: String,

    /// ?
    #[serde(rename = "Environment")]
    pub environment: Environment,

    /// Product data
    #[serde(rename = "Product")]
    pub product: Product,
}

/// Environment field
#[derive(Debug, serde::Deserialize)]
pub struct Environment {}

/// Product field
#[derive(Debug, serde::Deserialize)]
pub struct Product {
    /// The outline
    #[serde(rename = "Outline")]
    pub outline: Outline,
}

/// Outline field
#[derive(Debug, serde::Deserialize)]
pub struct Outline {
    /// The menu title
    pub menutitle: String,

    #[serde(rename = "TOC")]
    pub toc: Toc,
}

/// Toc field
#[derive(Debug, serde::Deserialize)]
pub struct Toc {
    /// TOC title
    pub title: String,

    /// cover image?
    pub href: String,

    /// Sections
    #[serde(rename = "Section", default)]
    pub sections: Vec<Section>,
}

impl Toc {
    /// Iter over all sections.
    pub fn iter_all_sections(&self) -> impl Iterator<Item = &Section> + '_ {
        let capacity = 1 + self
            .sections
            .iter()
            .map(|section| section.depth())
            .max()
            .unwrap_or(0);
        let mut stack = Vec::with_capacity(capacity);
        stack.push(self.sections.iter());

        std::iter::from_fn(move || loop {
            let maybe_next = stack.last_mut()?.next();
            match maybe_next {
                Some(next) => {
                    stack.push(next.sections.iter());
                    return Some(next);
                }
                None => {
                    stack.pop();
                }
            }
        })
    }
}

/// A TOC section
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Section {
    /// Section index
    pub index: String,

    /// Section title
    pub title: String,

    /// Section cover image?
    pub href: String,

    /// Sub-Sections
    #[serde(rename = "Section", default)]
    pub sections: Vec<Section>,

    #[serde(rename = "Resource", default)]
    pub resources: Vec<Resource>,
}

impl Section {
    /// Get the number of layers of sections this contains.
    ///
    /// A section with no sub-sections has a depth of 1.
    pub fn depth(&self) -> usize {
        1 + self
            .sections
            .iter()
            .map(|section| section.depth())
            .max()
            .unwrap_or(0)
    }
}

/// A resource
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Resource {
    /// Resource index
    pub index: String,

    /// Resource title
    pub title: String,

    /// Resource cover image?
    pub href: String,
}

impl Resource {
    /// Returns true if this is an exam.
    pub fn is_exam(&self) -> bool {
        self.href.ends_with(".exam.xml")
    }
}
