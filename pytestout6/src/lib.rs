use num_traits::cast::FromPrimitive;
use pyo3::{
    exceptions::PyException,
    prelude::*,
};
use uuid::Uuid;

#[pyclass]
pub struct Client {
    client: testout6::Client,
    tokio_rt: tokio::runtime::Runtime,
}

#[pymethods]
impl Client {
    #[new]
    fn new() -> PyResult<Self> {
        let client = testout6::Client::new();
        let tokio_rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .map_err(|_e| PyException::new_err("failed to build tokio runtime".to_string()))?;

        Ok(Client { client, tokio_rt })
    }

    pub fn login(&self, username: &str, password: &str) -> PyResult<LoginResponse> {
        self.tokio_rt
            .block_on(self.client.login(username, password))
            .map(LoginResponse)
            .map_err(|e| PyException::new_err(e.to_string()))
    }

    pub fn get_activated_products_and_classes(
        &self,
        user_id: &str,
    ) -> PyResult<ActivatedProductsAndClassesResponse> {
        let user_id = Uuid::parse_str(user_id).map_err(|e| PyException::new_err(e.to_string()))?;

        self.tokio_rt
            .block_on(self.client.get_activated_products_and_classes(user_id))
            .map(ActivatedProductsAndClassesResponse)
            .map_err(|e| PyException::new_err(e.to_string()))
    }

    pub fn get_outline(
        &self,
        major_version: u32,
        minor_version: u32,
        patch_version: u32,
        locale: &str,
        href: &str,
    ) -> PyResult<OutlineNavigation> {
        self.tokio_rt
            .block_on(self.client.get_outline(
                major_version,
                minor_version,
                patch_version,
                locale,
                href,
            ))
            .map(OutlineNavigation)
            .map_err(|e| PyException::new_err(e.to_string()))
    }

    pub fn get_resource_map(
        &self,
        default_version_id: u32,
        patch_version: u32,
    ) -> PyResult<ResourceMap> {
        self.tokio_rt
            .block_on(
                self.client
                    .get_resource_map(default_version_id, patch_version),
            )
            .map(ResourceMap)
            .map_err(|e| PyException::new_err(e.to_string()))
    }

    pub fn get_exam_attempts(
        &self,
        user_id: &str,
        resource_id: &str,
    ) -> PyResult<Vec<ExamAttempt>> {
        let user_id = Uuid::parse_str(user_id).map_err(|e| PyException::new_err(e.to_string()))?;
        let resource_id =
            Uuid::parse_str(resource_id).map_err(|e| PyException::new_err(e.to_string()))?;

        self.tokio_rt
            .block_on(self.client.get_exam_attempts(user_id, resource_id))
            .map(|attempts| attempts.into_iter().map(ExamAttempt).collect())
            .map_err(|e| PyException::new_err(e.to_string()))
    }

    pub fn create_exam(
        &self,
        user_id: &str,
        default_version_id: u32,
        resource_id: &str,
    ) -> PyResult<CreateExamResponse> {
        let user_id = Uuid::parse_str(user_id).map_err(|e| PyException::new_err(e.to_string()))?;
        let resource_id =
            Uuid::parse_str(resource_id).map_err(|e| PyException::new_err(e.to_string()))?;

        self.tokio_rt
            .block_on(
                self.client
                    .create_exam(user_id, default_version_id, resource_id),
            )
            .map(CreateExamResponse)
            .map_err(|e| PyException::new_err(e.to_string()))
    }

    pub fn submit_result(
        &self,
        user_id: &str,
        submit_result_request: &SubmitResultRequest,
    ) -> PyResult<()> {
        let user_id = Uuid::parse_str(user_id).map_err(|e| PyException::new_err(e.to_string()))?;

        self.tokio_rt
            .block_on(self.client.submit_result(user_id, &submit_result_request.0))
            .map(|_response| ())
            .map_err(|e| PyException::new_err(e.to_string()))
    }
}

#[pyclass]
pub struct LoginResponse(testout6::LoginResponse);

#[pymethods]
impl LoginResponse {
    pub fn is_successful(&self) -> bool {
        self.0.success
    }

    pub fn get_user_id(&self) -> Option<String> {
        Some(format!(
            "{}",
            self.0
                .authenticated_user
                .as_ref()?
                .user_profile
                .user_profile_id
        ))
    }
}

#[pyclass]
pub struct OutlineNavigation(testout6::OutlineNavigation);

#[pymethods]
impl OutlineNavigation {
    pub fn get_all_sections(&self) -> Vec<OutlineSection> {
        self.0
            .product
            .outline
            .toc
            .iter_all_sections()
            .cloned()
            .map(OutlineSection)
            .collect()
    }
}

#[pyclass]
pub struct ResourceMap(testout6::ResourceMap);

#[pymethods]
impl ResourceMap {
    pub fn get(&self, href: &str) -> Option<ResourceMapEntry> {
        self.0.get(href).cloned().map(ResourceMapEntry)
    }
}

#[pyclass]
pub struct ResourceMapEntry(testout6::ResourceMapEntry);

#[pymethods]
impl ResourceMapEntry {
    pub fn is_exam(&self) -> bool {
        self.0.resource_type == testout6::ResourceType::Exam
    }

    pub fn is_sim(&self) -> bool {
        self.0.resource_type == testout6::ResourceType::Sim
    }

    pub fn get_resource_id(&self) -> String {
        self.0.resource_id.to_string()
    }
}

#[pyclass]
pub struct ActivatedProductsAndClassesResponse(testout6::ActivatedProductsAndClassesResponse);

#[pymethods]
impl ActivatedProductsAndClassesResponse {
    pub fn get_activated_products(&self) -> PyResult<Vec<ActivatedProduct>> {
        Ok(self
            .0
            .activated_products
            .iter()
            .cloned()
            .map(ActivatedProduct)
            .collect())
    }
}

#[pyclass]
pub struct ExamAttempt(testout6::Attempt);

#[pymethods]
impl ExamAttempt {
    pub fn is_passed(&self) -> bool {
        self.0.passed
    }
}

#[pyclass]
pub struct CreateExamResponse(testout6::CreateExamResponse);

#[pymethods]
impl CreateExamResponse {
    pub fn get_exam_session_id(&self) -> u64 {
        self.0.exam_session_id
    }
}

#[pyclass]
pub struct ActivatedProduct(testout6::ActivatedProduct);

#[pymethods]
impl ActivatedProduct {
    pub fn get_default_available_version(&self) -> Option<AvailableVersion> {
        self.0
            .get_default_available_version()
            .cloned()
            .map(AvailableVersion)
    }

    pub fn get_default_product_version(&self) -> Option<ActivatedProductsAndClassesProductVersion> {
        self.0
            .get_default_product_version()
            .cloned()
            .map(ActivatedProductsAndClassesProductVersion)
    }

    pub fn get_locale(&self) -> &str {
        &self.0.product.locale
    }

    pub fn get_default_version_id(&self) -> u32 {
        self.0.default_version_id
    }
}

#[pyclass]
pub struct AvailableVersion(testout6::AvailableVersion);

#[pyclass]
pub struct ActivatedProductsAndClassesProductVersion(
    testout6::ActivatedProductsAndClassesProductVersion,
);

#[pymethods]
impl ActivatedProductsAndClassesProductVersion {
    pub fn get_major_version(&self) -> u32 {
        self.0.major_version
    }

    pub fn get_minor_version(&self) -> u32 {
        self.0.minor_version
    }

    pub fn get_patch_version(&self) -> u32 {
        self.0.patch_version
    }

    pub fn get_outline_href(&self) -> &str {
        &self.0.outline_href
    }
}

#[pyclass]
pub struct OutlineSection(testout6::OutlineSection);

#[pymethods]
impl OutlineSection {
    pub fn get_resources(&self) -> Vec<OutlineResource> {
        self.0
            .resources
            .iter()
            .cloned()
            .map(OutlineResource)
            .collect()
    }
}

#[pyclass]
pub struct OutlineResource(testout6::OutlineResource);

#[pymethods]
impl OutlineResource {
    pub fn get_href(&self) -> &str {
        &self.0.href
    }

    pub fn get_index(&self) -> &str {
        self.0.index.as_str()
    }
}

#[pyclass]
pub struct SubmitResultRequest(testout6::SubmitResultRequest);

#[pymethods]
impl SubmitResultRequest {
    #[allow(clippy::too_many_arguments)]
    #[new]
    pub fn new(
        user_profile_id: &str,
        resource_id: &str,
        resource_type: u8,
        resource_sub_type: u8,
        group_id: u32,
        points_scored: u8,
        points_possible: u8,
        passed: bool,
        seconds_in_resource: u64,
        response_details: u64,
        exam_session_id: u64,
    ) -> PyResult<Self> {
        let user_profile_id =
            Uuid::parse_str(user_profile_id).map_err(|e| PyException::new_err(e.to_string()))?;
        let resource_id =
            Uuid::parse_str(resource_id).map_err(|e| PyException::new_err(e.to_string()))?;
        let resource_type = testout6::ResourceType::from_u8(resource_type)
            .ok_or_else(|| PyException::new_err("invalid `resource_type`"))?;
        let resource_sub_type = testout6::ResourceType::from_u8(resource_sub_type)
            .ok_or_else(|| PyException::new_err("invalid `sub_resource_type`"))?;
        let group_id = group_id.to_string();

        Ok(Self(testout6::SubmitResultRequest {
            user_profile_id,
            resource_id,
            resource_type,
            resource_sub_type,
            group_id,
            points_scored,
            points_possible,
            passed,
            seconds_in_resource,
            response_details,
            active_class_profile_id: None,
            exam_session_id,
        }))
    }
}

#[pymodule]
fn pytestout6(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Client>()?;
    m.add_class::<SubmitResultRequest>()?;
    Ok(())
}
