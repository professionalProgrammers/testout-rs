use crate::types::{
    Command,
    User,
};
use serde::{
    Deserialize,
    Serialize,
};

#[derive(Debug, Serialize, Deserialize)]
pub enum ApiResponse {
    InitiateClientHandshakeResult {
        #[serde(rename = "checkOffset")]
        check_offset: u32,
        result: bool,
        #[serde(rename = "serviceId")]
        service_id: String, //Also GUID, maybe serdeify?
    },
    CompleteClientHandshakeResult(bool),
    AuthenticateResult(AuthenticateResult),
    SendReceiveResult(SendReceiveResult),
    CreateLabSessionResult(String),
    PostLabSessionOpenedResult,
    GetLabSessionStatusResult {
        #[serde(alias = "Opened")]
        opened: bool,
        #[serde(alias = "Response")]
        response: serde_json::Value,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthenticateResult {
    #[serde(rename = "AccountDisabled")]
    account_disabled: bool,
    #[serde(rename = "Authenticated")]
    pub authenticated: bool,
    #[serde(rename = "AuthorizedMachine")]
    authorized_machine: bool,
    #[serde(rename = "ErrorMessage")]
    error_message: String,
    #[serde(rename = "ExceededLogins")]
    exceeded_logins: bool,
    #[serde(rename = "GraceLoginsRemaining")]
    grace_logins_remaining: u32,
    #[serde(rename = "IsTestOutIP")]
    is_testout_ip: bool,
    #[serde(rename = "NeedEmail")]
    need_email: bool,
    #[serde(rename = "NeedNewPassword")]
    need_new_password: bool,
    #[serde(rename = "NeedSecurityQuestions")]
    need_security_questions: bool,
    #[serde(rename = "PreferredClient")]
    preferred_client: String,
    #[serde(rename = "ServerDateTime")]
    server_date_time: String,
    #[serde(rename = "ServiceId")]
    service_id: String,
    #[serde(rename = "TemporaryLockout")]
    temporary_lockout: bool,
    #[serde(rename = "User")]
    pub user: User,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SendReceiveResult {
    #[serde(rename = "Commands")]
    pub commands: Vec<Command>,
    #[serde(rename = "MoreObjectsInQueue")]
    more_objects_in_queue: bool,
}
