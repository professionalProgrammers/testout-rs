/// Library types
pub mod types;

use crate::types::LoginRequestJson;
pub use crate::types::{
    ActivatedProduct,
    ActivatedProductsAndClassesProduct,
    ActivatedProductsAndClassesProductVersion,
    ActivatedProductsAndClassesResponse,
    Attempt,
    AvailableVersion,
    CreateExamResponse,
    LoginResponse,
    Outline,
    OutlineNavigation,
    OutlineResource,
    OutlineSection,
    ResourceMapEntry,
    ResourceType,
    SubmitResultRequest,
    TabResponse,
};
use serde_json::json;
use sha2::{
    Digest,
    Sha256,
};
use std::{
    collections::HashMap,
    sync::Arc,
};
use time::{
    format_description::well_known::Rfc3339,
    OffsetDateTime,
};
use tokio::sync::Semaphore;
use uuid::Uuid;

pub type ResourceMap = HashMap<String, ResourceMapEntry>;

/// Library error type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Reqwest Error
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    /// Quick Xml De Error
    #[error(transparent)]
    QuickXmlDe(#[from] quick_xml::DeError),

    /// Tokio Join Error
    #[error(transparent)]
    TokioJoin(#[from] tokio::task::JoinError),

    /// A semaphore was closed
    #[error(transparent)]
    SemaphoreClosed(#[from] tokio::sync::AcquireError),

    /// Json Error
    #[error(transparent)]
    Json(#[from] serde_json::Error),

    /// Time Formatting error
    #[error(transparent)]
    TimeFormat(#[from] time::error::Format),
}

/// Client
#[derive(Debug, Clone)]
pub struct Client {
    client: reqwest::Client,
    spawn_blocking_semaphore: Arc<Semaphore>,
}

impl Client {
    /// Make a new [`Client`].
    pub fn new() -> Self {
        Self {
            client: reqwest::Client::builder()
                .cookie_store(true)
                .build()
                .expect("failed to build testout client"),
            // TODO: Allow user config
            spawn_blocking_semaphore: Arc::new(Semaphore::new(4)),
        }
    }

    /// Log in this client.
    pub async fn login(&self, username: &str, password: &str) -> Result<LoginResponse, Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        let url = format!(
            "https://labsimapi.testout.com/api/v1/authentication/signin_credentials?cacheBust={cache_bust}",
            cache_bust = cache_bust
        );

        // Hash password
        let mut hasher = Sha256::new();
        hasher.update(password);
        let password_hash = hasher.finalize();

        // Send request and parse response
        Ok(self
            .client
            .post(url)
            .json(&LoginRequestJson {
                login: username,
                password_hash: format!("{:x}", password_hash),
                trust_device: false,
                two_factor_code: "",
            })
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Get activated products and classes.
    pub async fn get_activated_products_and_classes(
        &self,
        user_id: Uuid,
    ) -> Result<ActivatedProductsAndClassesResponse, Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        let future_classes = true;
        let url = format!(
            "https://labsimapi.testout.com/api/v1/users/{user_id}/activatedproductsandclasses?futureClasses={future_classes}&includebeta=true&showArchived=false&cacheBust={cache_bust}", 
            user_id = user_id,
            future_classes = future_classes,
            cache_bust = cache_bust,
        );

        // Send request and parse response
        Ok(self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Get an outline
    pub async fn get_outline(
        &self,
        major_version: u32,
        minor_version: u32,
        patch_version: u32,
        locale: &str,
        outline: &str,
    ) -> Result<OutlineNavigation, Error> {
        // Make url
        let outline = outline.to_lowercase();
        let url = format!(
            "https://cdn.testout.com/_version_{major_version}{minor_version}{patch_version}/{outline_trim}-{locale}/{locale}/{outline}", 
            major_version = major_version,
            minor_version = minor_version,
            patch_version = patch_version,
            locale = locale,
            outline_trim = outline.trim_end_matches(".xml"), 
            outline = outline
        );

        // Send request and parse response
        let text = self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;
        let _ = self.spawn_blocking_semaphore.acquire().await?;
        Ok(tokio::task::spawn_blocking(move || quick_xml::de::from_str(&text)).await??)
    }

    /// Get resource map
    pub async fn get_resource_map(
        &self,
        product_version: u32,
        patch_version: u32,
    ) -> Result<HashMap<String, ResourceMapEntry>, Error> {
        // Make url
        //
        // the deployment type.
        // staging = 1
        // live = 2
        let deployment_type = 2;
        let url = format!(
            "https://labsimapi.testout.com/api/v1/resources/versions/{product_version}/{deployment_type}/map?patchVersionId={patch_version}", 
            product_version = product_version,
            deployment_type = deployment_type,
            patch_version = patch_version,
        );

        // Send request and parse response
        Ok(self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Get exam attempts
    pub async fn get_exam_attempts(
        &self,
        user_id: Uuid,
        resource_id: Uuid,
    ) -> Result<Vec<Attempt>, Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        let url = format!(
            "https://labsimapi.testout.com/api/v1/resourceResults/{user_id}/labsimsaas_results2/{resource_id}/attempts?cacheBust={cache_bust}", 
            user_id = user_id,
            resource_id = resource_id,
            cache_bust = cache_bust,
        );

        // Send request and parse response
        Ok(self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Get resource info
    pub async fn get_resource_info(
        &self,
        product_version: u32,
        resource_id: Uuid,
    ) -> Result<serde_json::Value, Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        // the deployment type.
        // staging = 1
        // live = 2
        let deployment_type = 2;
        let url = format!(
            "https://labsimapi.testout.com/api/v1/resources/versions/{product_version}/{deployment_type}/{resource_id}?cacheBust={cache_bust}",
            product_version = product_version,
            deployment_type = deployment_type,
            resource_id = resource_id,
            cache_bust = cache_bust,
        );

        // Send request and parse response
        Ok(self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Get info about a tab or create a tab if the given tab id does not exist.
    ///
    /// # Returns
    /// Returns [`None`] if a new tab was created.
    pub async fn get_tab_info(
        &self,
        tab_id: Uuid,
        user_id: Uuid,
    ) -> Result<Option<serde_json::Value>, Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        let url = format!(
            "https://labsimapi.testout.com/api/v1/tabinterops/{tab_id}/{user_id}?cacheBust={cache_bust}",
            tab_id = tab_id,
            user_id = user_id,
            cache_bust = cache_bust,
        );

        // Send request and parse response
        let res = self.client.get(url).send().await?.error_for_status()?;
        let status = res.status();
        // Check if a new tab was created
        if status == reqwest::StatusCode::NO_CONTENT {
            return Ok(None);
        }
        Ok(res.json().await?)
    }

    /// Create an exam.
    pub async fn create_exam(
        &self,
        user_id: Uuid,
        product_version: u32,
        resource_id: Uuid,
    ) -> Result<CreateExamResponse, Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        let url = format!(
            "https://labsimapi.testout.com/api/v1/examsessions/{user_id}/labsimsaas_results2?cacheBust={cache_bust}",
            user_id = user_id,
            cache_bust = cache_bust,
        );

        // Send request and parse response
        let now = OffsetDateTime::now_utc();
        let now_month = now.month();
        let end = time::PrimitiveDateTime::new(
            time::Date::from_calendar_date(
                now.year()
                    + if now_month == time::Month::December {
                        1
                    } else {
                        0
                    },
                now_month.next(),
                now.day(),
            )
            .expect("invalid offset date"),
            now.time(),
        )
        .assume_offset(time::UtcOffset::UTC);
        Ok(self
            .client
            .post(url)
            .json(&json!({
                "userProfileId": user_id,
                "productVersionId": product_version,
                "outlineResourceId": resource_id,
                "examFormResourceId": serde_json::Value::Null,
                "scaleKey": serde_json::Value::Null,
                "availabilityStartDateUtc": now.format(&Rfc3339)?,
                "availabilityEndDateUtc": end.format(&Rfc3339)?,
            }))
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Submit a tab response
    pub async fn submit_tab_response(
        &self,
        tab_id: Uuid,
        user_id: Uuid,
        tab_response: &TabResponse,
    ) -> Result<(), Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        let url = format!(
            "https://labsimapi.testout.com/api/v1/tabinterops/{tab_id}/{user_id}/tabresponse?cacheBust={cache_bust}",
            tab_id = tab_id,
            user_id = user_id,
            cache_bust = cache_bust,
        );

        // Send request and parse response
        let response_str = serde_json::to_string(tab_response)?;
        let response_compressed = lz_str::compress_to_encoded_uri_component(response_str.as_str());
        self.client
            .put(url)
            .json(&json!({
                "response": format!("POSTED::{}", response_compressed),
            }))
            .send()
            .await?
            .error_for_status()?;

        Ok(())
    }

    /// Submit a result
    pub async fn submit_result(
        &self,
        user_id: Uuid,
        request: &SubmitResultRequest,
    ) -> Result<serde_json::Value, Error> {
        // Make url
        let cache_bust = Uuid::new_v4();
        let url = format!(
            "https://labsimapi.testout.com/api/v1/resourceResults/{user_id}/labsimsaas_results2/encoded?cacheBust={cache_bust}", 
            user_id = user_id,
            cache_bust = cache_bust,
        );

        // Send request and parse response
        let request_str = serde_json::to_string(request)?;
        let request_compressed = lz_str::compress_to_encoded_uri_component(request_str.as_str());
        Ok(self
            .client
            .post(url)
            .json(&json!({ "value": request_compressed }))
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}
