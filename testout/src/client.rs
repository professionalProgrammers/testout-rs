use crate::{
    guid::Guid,
    types::{
        response::{
            ApiResponse,
            AuthenticateResult,
            SendReceiveResult,
        },
        Command,
        CommandList,
    },
    PostLabScoreRequest,
    ProductData,
    TestoutError,
    TestoutResult,
};
use http::Method;
use reqwest::{
    header::{
        CONTENT_TYPE,
        USER_AGENT,
    },
    Client as ReqwestClient,
    Response,
};
use sha2::{
    Digest,
    Sha256,
};
use std::borrow::Cow;

const USER_AGENT_STR: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36";

#[derive(Debug)]
pub struct Request<'a> {
    pub url: Cow<'a, str>,
    pub data: Vec<u8>,
}

pub struct Client {
    handle: ReqwestClient,
    service_id: Option<String>,
}

impl Client {
    /// Make a new client
    pub fn new() -> Self {
        Client {
            handle: ReqwestClient::new(),
            service_id: None,
        }
    }

    pub async fn send_req(&self, req: Request<'_>) -> TestoutResult<Response> {
        let guid = Guid::from_random(false);

        let res = self
            .handle
            .request(Method::OPTIONS, &format!("{}?{}", &*req.url, guid))
            .header(USER_AGENT, USER_AGENT_STR)
            .send()
            .await?
            .error_for_status()?;

        let res = self
            .handle
            .request(Method::POST, &format!("{}?{}", &*req.url, guid))
            .header(USER_AGENT, USER_AGENT_STR)
            .header(CONTENT_TYPE, "application/json")
            .body(req.data)
            .send()
            .await?
            .error_for_status()?;

        Ok(res)
    }

    pub async fn send_req_api(&self, req: Request<'_>) -> TestoutResult<ApiResponse> {
        let res = self.send_req(req).await?;
        let text = res.text().await.map_err(|_| TestoutError::Network)?;

        match serde_json::from_str(&text) {
            Ok(v) => Ok(v),
            Err(e) => Err(TestoutError::JsonParse(format!("{}", e), text)),
        }
    }

    pub async fn send_req_text(&self, req: Request<'_>) -> TestoutResult<String> {
        self.send_req(req)
            .await?
            .text()
            .await
            .map_err(|_| TestoutError::InvalidBody)
    }

    pub async fn send_request(&self, req: Request<'_>) -> TestoutResult<ApiResponse> {
        self.send_req_api(req).await
    }

    pub async fn handshake(&mut self) -> TestoutResult<(ApiResponse, ApiResponse)> {
        let req = Request {
			url: "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/InitiateClientHandshake".into(),
			data: br#"{"clientType":"LabSim5","clientVersion":"1.0.0","machineId":"clientversion=labsim5;html5=true;os=Windows;browser=Chrome;browserversion=72.0.3626.121;browserculture=en-us;labsimrelease=5.1.10.553","checkVal":0}"#.to_vec(),
		};

        let init_handshake = self.send_req_api(req).await?;
        match init_handshake {
            ApiResponse::InitiateClientHandshakeResult { ref service_id, .. } => {
                self.service_id = Some(service_id.clone());
            }
            _ => return Err(TestoutError::InvalidApiResponse),
        };

        let req = Request {
			url: "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/CompleteClientHandshake".into(),
			data: format!(r#"{{"serviceId":"{}","checkVal":0}}"#, self.service_id.as_ref().unwrap()).into_bytes(),
		};

        let complete_handshake = self.send_req_api(req).await?;

        match complete_handshake {
            ApiResponse::CompleteClientHandshakeResult(_ok) => {
                //assert_eq!(ok, true); //TODO: Validate?
            }
            _ => return Err(TestoutError::InvalidApiResponse),
        }

        Ok((init_handshake, complete_handshake))
    }

    pub async fn login(
        &mut self,
        username: &str,
        password: &str,
    ) -> TestoutResult<AuthenticateResult> {
        let hashed_password = format!("{:x}", Sha256::digest(password.as_bytes()));
        let authenticate_url =
            "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/Authenticate";

        let req = Request {
            url: authenticate_url.into(),
            data: format!(
                r#"{{"serviceId":"{}","loginStr":"{}","passwordHash":"{}"}}"#,
                self.service_id
                    .as_ref()
                    .ok_or(TestoutError::InvalidServiceId)?,
                username,
                hashed_password
            )
            .into_bytes(),
        };

        let auth_response = self.send_req_api(req).await?;

        match auth_response {
            ApiResponse::AuthenticateResult(auth_result) => Ok(auth_result),
            _ => Err(TestoutError::InvalidApiResponse),
        }
    }

    pub async fn create_lab_session(&mut self) -> TestoutResult<String> {
        let url =
            "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/CreateLabSession";

        let req = Request {
            url: url.into(),
            data: Vec::new(),
        };

        let response = self.send_req_api(req).await?;

        match response {
            ApiResponse::CreateLabSessionResult(key) => Ok(key),
            _ => Err(TestoutError::InvalidApiResponse),
        }
    }

    pub async fn post_lab_session_opened(&mut self, id: &str) -> TestoutResult<String> {
        let url =
            "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/PostLabSessionOpened";
        let req = Request {
            url: url.into(),
            data: format!(r#"{{"labSessionId": "{}"}}"#, id).into_bytes(),
        };

        self.send_req_text(req).await //Returns "{}". Json parsing fails as nothing in the enum matches. Not worth fixing as its empty anyways.
    }

    pub async fn get_lab_session_status(&mut self, id: &str) -> TestoutResult<ApiResponse> {
        let url =
            "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/GetLabSessionStatus";
        let req = Request {
            url: url.into(),
            data: format!(r#"{{"labSessionId": "{}"}}"#, id).into_bytes(),
        };

        let response = self.send_req_api(req).await?;

        match response {
            ApiResponse::GetLabSessionStatusResult { .. } => Ok(response),
            _ => Err(TestoutError::InvalidApiResponse),
        }
    }

    pub async fn post_lab_score(&mut self, res: &PostLabScoreRequest<'_>) -> TestoutResult<String> {
        let data = res
            .get_submit_str()
            .ok_or(TestoutError::RequestConstruction)?;

        let url =
            "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/PostLabScore";
        let req = Request {
            url: url.into(),
            data: format!(r#"{{"data": "{}"}}"#, data).into_bytes(),
        };

        self.send_req_text(req).await //Returns "<PostLabScoreResponse xmlns="http://tempuri.org/"/>" If ok. This is just stupid. Why would they randomy switch to json for ONE api call?
    }

    pub async fn post_lab_session_response(
        &mut self,
        id: &str,
        data: &str,
    ) -> TestoutResult<String> {
        let url =
            "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/PostLabSessionResponse";
        let req = Request {
            url: url.into(),
            data: format!(r#"{{"labSessionId": "{}", "response": "!{}"}}"#, id, data).into_bytes(),
        };

        self.send_req_text(req).await
    }

    pub async fn execute_commands(
        &self,
        commands: Vec<Command>,
    ) -> TestoutResult<SendReceiveResult> {
        let list = CommandList::new(self.service_id.clone().unwrap(), commands);
        let req = Request {
            url:
                "https://webservices.testout.com/OrbisServerJson/PortalServiceJson.svc/SendReceive"
                    .into(),
            data: serde_json::to_vec(&list).unwrap(),
        };

        //println!("{}", serde_json::to_string(&list).unwrap());

        let result = self.send_request(req).await?;
        match result {
            ApiResponse::SendReceiveResult(result) => Ok(result),
            _ => Err(TestoutError::InvalidApiResponse),
        }
    }

    pub async fn get_outline(&self, data: &ProductData) -> TestoutResult<Vec<u8>> {
        //https://cdn.testout.com/_version_602/pcpro2018v6-en-us/en-us/pcpro2018v6.xml?v=6.0.2
        let url = data.get_url().unwrap(); //TODO: remove unwraps

        let res = self
            .handle
            .get(&url)
            .send()
            .await
            .map_err(|_| TestoutError::Network)?; //Is a copy necessary?
        let buf: Vec<u8> = res
            .bytes()
            .await
            .map_err(|_| TestoutError::Network)?
            .to_vec();
        Ok(buf)
    }

    pub async fn get_string(&mut self, url: &str) -> TestoutResult<String> {
        self.handle
            .get(url)
            .send()
            .await
            .map_err(|_| TestoutError::Network)?
            .text()
            .await
            .map_err(|_| TestoutError::InvalidBody)
    }
}
