extern crate chrono;
extern crate lz_string;
extern crate ratel;
extern crate ratel_visitor;
extern crate serde_json;
extern crate testout;

use chrono::{
    DateTime,
    Local,
};
use serde_json::json;
use testout::{
    encode_datetime,
    Client,
    Guid,
    LabData,
    PostLabScoreRequest,
};

#[test]
fn rand_guid() {
    println!("{}", Guid::from_random(false));
    println!("{}", Guid::from_random(true));
}

#[test]
fn datetime() {
    let local: DateTime<Local> = Local::now();
    println!("{}", encode_datetime(&local));
}

#[test]
fn parse_xml() {
    let _file = std::fs::File::open("tests/pcpro2018v6.xml").unwrap();
}

//#[test]
fn client() {}

#[test]
fn lz_string_decompress() {
    let compressed = "N4IgrgzgpgTg4mAlgExALhMgHFArF5KAFgFoAzATgGMA2EosgJgAYSsBmKVmgRgEMeZKs3ZYqfdiAA0IAPZgALgBtEAOygAxREqjoQAByr6YsljywA3GgDoAHgFsl0kBCqyYagOYAFPgoAWevr6NAA6oRCI9hDhCgCe+lCuHvoKFjzhhsamzOZWmXxUANZ8nkmZRiZmljQA7mrIsrUQ+oUlZeFKAPq13dlk2lA8XcE2AFYQzoUKYHxKAMpuMLpozDL6shCRAEY6i+4rjDLQbqrIEACSqgBKSfIwVCvsAOwyhAp82pMYzAD1jL8XtYXhRfqwANR-AByfChvx4IAAvkA";
    let decompressed = lz_string::decompress_uri(&compressed).unwrap();
    println!("{}", &decompressed);
}

#[test]
pub fn lz_string_compress() {
    let json = json!({
       "userGuid":"d8e58de4-f9c6-4f20-83e0-61a1fc038ca3",
       "outlineFile": "pcpro2018v6.xml",
       "scoringPath": "pp6\\sims\\typescriptv1\\pcpro2018v6\\packages\\pcpro2018v6windowspackage\\l_wl_profile1_pp6.js",
       "actualScore": 0,
       "possibleScore": 2,
       "secondsInResource": 37,
       "details": "0¯2¯37.379¯0-+0¯NaN¯1"
    });

    let data = serde_json::to_string(&json).unwrap();
    let compressed = lz_string::compress_uri(&data).unwrap();
    dbg!(&compressed);
    let decompressed = lz_string::decompress_uri(&compressed);
    dbg!(&decompressed);
}

/*
N4IgrgzgpgTg4mAlgExALhMgHFArF5KAFgFoAzATgGMA2EosgJgAYSsBmKVmgRgEMeZKs3ZYqfdiAA0IAPZgALgBtEAOygAxREqjoQAByr6YsljywA3GgDoAHgFsl0kBCqyYagOYAFPgoAWevr6NAA6oRCI9hDhCgCe+lCuHvoKFjzhhsamzOZWmXxUANZ8nkmZRiZmljQA7mrIsrUQ+oUlZeFKAPpqCiY8XcE2AFYQzoUKYHxKAMpuMLpoRDL6shCRAEY6c+6Lyy5QbqrIEACSqgBKSfIwVItYNDKECnzaYxhEAPVfD9YUjIxPjxgTwANSgz4AOT4kKBIAAvkA
{"userGuid":"d8e58de4-f9c6-4f20-83e0-61a1fc038ca3","outlineFile":"pcpro2018v6.xml","scoringPath":"pp6\\sims\\typescriptv1\\pcpro2018v6\\packages\\pcpro2018v6windowspackage\\l_intro1_pp6.js","actualScore":4,"possibleScore":4,"secondsInResource":86,"details":"4¯4¯86.922¯1111++¯NaN¯1"}"
*/
/*
let mut cmd = Command::new(187);
cmd.set_param(String::from("entityGuid"), String::from("7b5510d0-5a0c-488c-9085-5986e2d11640"));
*/

/*
let mut cmd = Command::new(282); //187
cmd.set_param(String::from("entityGuid"), String::from("7b5510d0-5a0c-488c-9085-5986e2d11640"));
cmd.set_param(String::from("clientType"), String::from("HTML5"));
*/
