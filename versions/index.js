const https = require('https');
const querystring = require('querystring');

const testout = require('./testout/node');
const Guid = testout.Guid;
const Client = testout.Client;

function makeRequest(path, payload, cb) {
	const url = new URL(path, 'https://webservices.testout.com');
	url.search += 'bypassCache=' + Guid.fromRandom().toString();
	let opts = {
		method: 'OPTIONS',
		host: url.hostname,
		path: url.pathname + url.search,
		port: 443,
		headers: {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
			'Origin': 'https://cdn.testout.com',
			'Referer': 'https://cdn.testout.com/client-v5-1-10-553/startlabsim.html',
		},
	};

	//console.log(opts);
	let req = https.request(opts, function (res) {
			if (res.statusCode != 200) {
				console.log('Bad STATUS: ' + res.statusCode);
			}

			let buf = '';
			res.on('data', function (data) {
				buf += data;
			});

			res.on('end', function () {
				if (buf.length != 0) {
					console.log("Unexpected data: " + buf);
				}

				opts.method = 'POST';

				let qs = querystring.decode(url.search.substring(1));
				qs.bypassCache = Guid.fromRandom().toString();
				//console.log(JSON.stringify(qs));
				url.search = '?' + querystring.encode(qs);
				opts.headers['Content-Length'] = payload.length;
				opts.headers['Content-Type'] = 'application/json';
				opts.path = url.pathname + '?' + querystring.encode(qs);
				//console.log(opts);
				let req = https.request(opts, function (res) {
						console.log('STATUS: ' + res.statusCode);
						let buf = '';
						res.on('data', function (data) {
							buf += data;
						});

						res.on('end', function () {
							cb(buf);
						});
					});

				req.write(payload);
				req.end();
			});
		});

	req.end();
}

let send_recieve_url = '/OrbisServerJson/PortalServiceJson.svc/SendReceive';

let username = 'Venkata.nunduri';
let password = '9027552';

let client = new Client();

let handshake = client.handshake();
let serviceId = handshake[0].InitiateClientHandshakeResult.serviceId;
console.log("Service Id: " + serviceId);

let auth = client.login(username, password);
//uri = "https://cdn.testout.com/_version_602/pcpro2018v6-en-us/en-us/pcpro2018v6.xml?v=6.0.2

let sendRecieve1 = {
	serviceId,
	requestPackage: {
		"Commands": [{
				"ID": "accdc11d-ce52-4e72-8162-018c565d50e0",
				"Critical": false,
				"CommandType": 282,
				"Priority": 0,
				"Parameters": [{
						"Name": "entityGuid",
						"Value": "7b5510d0-5a0c-488c-9085-5986e2d11640"
					}, {
						"Name": "clientType",
						"Value": "HTML5"
					}
				],
				"Status": 0,
				"Objects": [],
				"SliceIndex": 0,
				"TotalObjectCount": 0,
				"ObjectsRemaining": 0
			}, {
				"ID": "d2fdaecd-7aa6-4b4f-8f7f-c395e11a5a5e",
				"Critical": false,
				"CommandType": 11,
				"Priority": 0,
				"Parameters": [{
						"Name": "userGuid",
						"Value": "d8e58de4-f9c6-4f20-83e0-61a1fc038ca3"
					}
				],
				"Status": 0,
				"Objects": [],
				"SliceIndex": 0,
				"TotalObjectCount": 0,
				"ObjectsRemaining": 0
			}
		],
		"MoreObjectsInQueue": false
	}
};

makeRequest(send_recieve_url, JSON.stringify(sendRecieve1), function (data) {
	console.log(data);
	let sendRecieve2 = {
		"serviceId": serviceId,
		"requestPackage": {
			"Commands": [{
					"ID": "e7971417-42ca-449a-957b-53cb2bb01886",
					"Critical": false,
					"CommandType": 281,
					"Priority": 2,
					"Parameters": [{
							"Name": "userGuid",
							"Value": "d8e58de4-f9c6-4f20-83e0-61a1fc038ca3"
						}
					],
					"Status": 0,
					"Objects": [],
					"SliceIndex": 0,
					"TotalObjectCount": 0,
					"ObjectsRemaining": 0
				}
			],
			"MoreObjectsInQueue": false
		}
	};
	makeRequest(send_recieve_url, JSON.stringify(sendRecieve2), function (data) {
		console.log(data);
		data = {
			"serviceId": "00e64b9d-327d-4215-accc-b111ab8862c9",
			"requestPackage": {
				"Commands": [{
						"ID": "e8e51df0-3cb1-4ea9-832e-71168215fef1",
						"Critical": true,
						"CommandType": 68,
						"Priority": 2,
						"Parameters": [{
								"Name": "userGuid",
								"Value": "d8e58de4-f9c6-4f20-83e0-61a1fc038ca3"
							}, {
								"Name": "outlineFile",
								"Value": "pcpro2018v6.xml"
							}, {
								"Name": "resourceType",
								"Value": "E"
							}, {
								"Name": "resourceName",
								"Value": "pp6\\resources\\exams\\e_installmem_pp6.exam.xml"
							}, {
								"Name": "resourceTitle",
								"Value": "Practice Questions"
							}, {
								"Name": "sectionIndex",
								"Value": "3.8.8"
							}, {
								"Name": "requiredScore",
								"Value": "0.8000"
							}, {
								"Name": "score",
								"Value": "0.9000"
							}, {
								"Name": "secondsInResource",
								"Value": "159"
							}, {
								"Name": "resultDateTime",
								"Value": "7e3312000210000"
							}
						],
						"Status": 0,
						"Objects": [],
						"SliceIndex": 0,
						"TotalObjectCount": 0,
						"ObjectsRemaining": 0
					}
				],
				"MoreObjectsInQueue": false
			}
		};
		//makeRequest(send_recieve_url, JSON.stringify(data), function (data) {
		//	console.log(data);
		//});
	});
});
