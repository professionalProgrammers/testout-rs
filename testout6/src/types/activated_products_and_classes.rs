use std::collections::HashMap;
use uuid::Uuid;

/// Activated Products and classes
#[derive(Debug, serde::Deserialize)]
pub struct ActivatedProductsAndClassesResponse {
    /// ?
    #[serde(rename = "acceleratorAddresses")]
    pub accelerator_addresses: Vec<serde_json::Value>,

    /// Activated Products
    #[serde(rename = "activatedProducts")]
    pub activated_products: Vec<ActivatedProduct>,

    //betaProducts: null
    //campuses: [,…]
    //classLinkingSkippedProductIds: []
    //otherProducts: []
    /// ?
    #[serde(rename = "serverCurrentDateUtc")]
    pub server_current_date_utc: String,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// Activated Product
#[derive(Debug, Clone, serde::Deserialize)]
pub struct ActivatedProduct {
    /// ?
    #[serde(rename = "availableVersions")]
    pub available_versions: Vec<AvailableVersion>,

    /// ?
    #[serde(rename = "campusProfileId")]
    pub campus_profile_id: Option<Uuid>,

    /// This is the default version id
    #[serde(rename = "defaultVersionId")]
    pub default_version_id: u32,

    /// ?
    #[serde(rename = "expirationDateUtc")]
    pub expiration_date_utc: String,

    /// Whether this is academic
    #[serde(rename = "isAcademic")]
    pub is_academic: bool,

    /// ?
    #[serde(rename = "isTempAccess")]
    pub is_temp_access: bool,

    /// Product data
    pub product: Product,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

impl ActivatedProduct {
    /// Get the default available version
    pub fn get_default_available_version(&self) -> Option<&AvailableVersion> {
        self.available_versions.iter().find(|available_version| {
            available_version.available_version_id == self.default_version_id
        })
    }

    /// Get the default product version
    pub fn get_default_product_version(&self) -> Option<&ProductVersion> {
        self.product
            .product_version
            .iter()
            .find(|el| el.product_version_id == self.default_version_id)
    }
}

/// Available activated product version
#[derive(Debug, Clone, serde::Deserialize)]
pub struct AvailableVersion {
    /// The available version id
    #[serde(rename = "availableVersionId")]
    pub available_version_id: u32,

    /// Class info
    #[serde(rename = "classInfo")]
    pub class_info: Option<Vec<ClassInfo>>,

    /*
    deprecationDateUtc: null
    */
    /// Whether the user is in this class
    #[serde(rename = "isInClass")]
    pub is_in_class: bool,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// Class Info
#[derive(Debug, Clone, serde::Deserialize)]
pub struct ClassInfo {
    /// Class Name
    #[serde(rename = "className")]
    pub class_name: String,

    /// ?
    #[serde(rename = "classProfileId")]
    pub class_profile_id: Uuid,

    /// Teacher names
    #[serde(rename = "teacherNames")]
    pub teacher_names: Vec<Vec<String>>,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// Product data
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Product {
    /// ?
    #[serde(rename = "isAutomationProduct")]
    pub is_automation_product: bool,

    /// ?
    pub isbn: String,

    /// Course locale
    pub locale: String,

    //productId: 266
    //productType: "PRODUCT"
    /// Past and present product data info
    #[serde(rename = "productVersion")]
    pub product_version: Vec<ProductVersion>,

    /// Product title
    pub title: String,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// Product version field
#[derive(Debug, Clone, serde::Deserialize)]
pub struct ProductVersion {
    /// ?
    #[serde(rename = "betaForEveryone")]
    pub beta_for_everyone: bool,

    /// ?
    #[serde(rename = "deprecationDateUtc")]
    pub deprecation_date_utc: Option<String>,

    /// ?
    #[serde(rename = "examPrep")]
    pub exam_prep: String,

    /// Major version number
    #[serde(rename = "majorVersion")]
    pub major_version: u32,

    /// Minor version number
    #[serde(rename = "minorVersion")]
    pub minor_version: u32,

    /// Outline file name
    #[serde(rename = "outlineHref")]
    pub outline_href: String,

    /// ?
    #[serde(rename = "outlineType")]
    pub outline_type: u32,

    /// ?
    #[serde(rename = "patchVersion")]
    pub patch_version: u32,

    /// ?
    #[serde(rename = "productId")]
    pub product_id: u32,

    /// ?
    #[serde(rename = "productVersionId")]
    pub product_version_id: u32,

    /*
    releaseDateUtc: "2017-10-02T18:04:30Z"
    versionStatus: "LIVE"
        */
    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}
