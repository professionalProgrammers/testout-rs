#[macro_use]
extern crate neon;
extern crate neon_serde;
extern crate testout;

use neon::prelude::*;
use testout::{
	Guid,
	Client
};

declare_types! {
    pub class JsGuid for Guid {
        init(mut cx) {
            let input: String = cx.argument::<JsString>(0)?.value();

            Ok(Guid::new(Some(&input)).expect("Invalid Input"))
        }

        method toString(mut cx) {
            let this = cx.this();
            let name = {
                let guard = cx.lock();
                let guid = this.borrow(&guard);
                format!("{}", *guid)
            };
            Ok(cx.string(&name).upcast())
        }
		
		static hi(mut cx){
			Ok(cx.string("what").upcast())
		}
    }
	
	pub class JsClient for Client {
		init(_cx) {
			Ok(Client::new())
		}
		
		method handshake(mut cx){
			let mut this = cx.this();
            let data = {
                let guard = cx.lock();
                let mut client = this.borrow_mut(&guard);
                client.handshake().unwrap()
            };
			
			Ok(neon_serde::to_value(&mut cx, &data)?)
		}
		
		method login(mut cx){
			let mut this = cx.this();
			let username: String = cx.argument::<JsString>(0)?.value();
			let password: String = cx.argument::<JsString>(1)?.value();
			
            let data = {
                let guard = cx.lock();
                let mut client = this.borrow_mut(&guard);
                client.login(&username, &password).unwrap()
            };
			Ok(neon_serde::to_value(&mut cx, &data)?)
		}
	}
}

fn guid_from_random(mut cx: FunctionContext) -> JsResult<JsGuid> {
    let exclude_dashes = cx
		.argument_opt(0)
		.map(|a| a.downcast::<JsBoolean>().unwrap().value())
		.unwrap_or(false);
	
    let args = vec![cx.string(Guid::from_random(exclude_dashes).value)];
    JsGuid::new(&mut cx, args)
}

fn hello(mut cx: FunctionContext) -> JsResult<JsString> {
    Ok(cx.string("hello node"))
}

register_module!(mut cx, {
    cx.export_class::<JsGuid>("Guid")?;
	cx.export_class::<JsClient>("Client")?;
    cx.export_function("hello", hello)?;
    cx.export_function("guidFromRandom", guid_from_random)
});
