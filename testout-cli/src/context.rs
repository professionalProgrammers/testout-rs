use crate::{
    entry::{
        extract_exam_data,
        iter_entry,
        iter_entry_ref,
        Entry,
    },
    print_command_parameters,
};
use anyhow::Context as _;
use rand::Rng;
use testout::{
    types::{
        response::AuthenticateResult,
        Command,
        Object,
    },
    Client,
    LabData,
    PostLabScoreRequest,
    ProductData,
};

pub struct Context {
    pub client: Client,
    pub user_id: Option<String>,
}

impl Context {
    /// Make a new cli context
    pub fn new() -> Self {
        Context {
            client: Client::new(),
            user_id: None,
        }
    }

    /// Log in this context
    pub async fn login(&mut self, user: &str, pass: &str) -> anyhow::Result<AuthenticateResult> {
        println!("Initalizing Handshake..."); // TODO: More error checks
        let _handshake = self.client.handshake().await.context("handshake failed")?;
        println!("Handshake Complete");

        println!("Logging in...");
        let login = self.client.login(user, pass).await?;
        if !login.authenticated {
            anyhow::bail!("login failed");
        }

        let user_id = login.user.id.clone();
        println!("Logged in. User Id: {}", user_id);
        self.user_id = Some(user_id);

        Ok(login)
    }

    pub async fn get_product_ids(&mut self) -> Result<String, ()> {
        let user_id = self
            .user_id
            .as_ref()
            .ok_or_else(|| println!("Error: User Id is not set!"))?;

        println!("Getting Product Ids (Command 249)...");
        let mut command_249 = Command::new(249);
        command_249.set_param(String::from("key"), user_id.clone());
        command_249.set_param(String::from("filter"), String::from(""));
        let command_vec = vec![command_249];
        let command_249_response = self
            .client
            .execute_commands(command_vec)
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;
        match command_249_response.commands[0].objects[2] {
            Object::Object8 { ref properties, .. } => {
                //TODO: Study effects of multiple products, better error checking, more data extraction
                let id = properties["CDIDs"].clone();
                println!("Got Product Id: {}", id);
                Ok(id)
            }
            _ => {
                println!("Error: Unexpected Object Type");
                return Err(());
            }
        }
    }

    pub async fn get_product_data(&mut self, product_id: &str) -> Result<ProductData, ()> {
        println!("Downloading Product Data (Command 29)...");
        let mut command_29 = Command::new(29);
        command_29.set_param(String::from("key"), String::from(""));
        command_29.set_param(String::from("filter"), String::from(""));
        let command_vec = vec![command_29];
        let command_29_response = self
            .client
            .execute_commands(command_vec)
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;

        let properties = command_29_response.commands[0]
            .objects
            .iter()
            .find_map(|obj| match obj {
                Object::Object8 { properties, .. } => {
                    if properties["CDId"] == product_id {
                        return Some(properties);
                    }
                    return None;
                }
                _ => None,
            })
            .ok_or_else(|| println!("Error: Product Data not located"))?;

        let data = ProductData::new(
            properties["OutlineFile"].clone(),
            properties["Version"].clone(),
            properties["Language"].clone(),
        );
        println!("Downloaded Product Data: {:#?}", data);

        Ok(data)
    }

    pub async fn download_and_parse_outline(
        &mut self,
        data: &ProductData,
    ) -> Result<Vec<Entry>, ()> {
        println!("Downloading outline file '{}'...", data.outline_file);
        let outline_xml = self
            .client
            .get_outline(&data)
            .await
            .map_err(|e| println!("Error: {:#?}", e))
            .map(String::from_utf8)?
            .map_err(|e| println!("Error: {:#?}", e))?;

        println!("Downloaded outline file '{}'", data.outline_file);

        println!("Parsing outline file..");
        let exam_data = extract_exam_data(&outline_xml).map_err(|s| println!("Error: {}", s))?;
        println!("Parsed outline file");
        let exam_data_filtered = exam_data
            .iter()
            .map(iter_entry_ref)
            .flatten()
            .cloned()
            .collect::<Vec<_>>();

        println!("# of entries: {}", exam_data_filtered.len());
        Ok(exam_data_filtered)
    }

    pub async fn complete_lab(&mut self, product_data: &ProductData, url: &str) -> Result<(), ()> {
        let user_id = self
            .user_id
            .as_ref()
            .ok_or_else(|| println!("Error: User Id is not set!"))?;

        let file_base = product_data
            .get_file_base()
            .ok_or_else(|| println!("Error: Invalid Outline File!"))?;

        let version_stripped = product_data.get_version_stripped();

        let target_url = format!(
            "https://cdn.testout.com/_version_{version_stripped}/{file_base}-{lang}/{lang}/{url}",
            version_stripped = version_stripped,
            file_base = file_base,
            lang = product_data.language,
            url = url
        );

        println!("Downloading Lab Data... ({})", target_url);
        let lab_data = self
            .client
            .get_string(&target_url)
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;
        println!("Downloaded Lab Data");

        println!("Parsing Lab Data...");
        let lab_data =
            LabData::from_str(&lab_data).ok_or_else(|| println!("Error: Invalid Lab Data"))?;
        println!("Parsed Lab Data");
        println!("# of tasks: {}", lab_data.task_count);

        println!("Generating Time Value...");
        let mut rng = rand::thread_rng();
        let time = rng.gen_range(60.0 * 2.0..60.0 * 10.0);
        println!("Generated Time Value: {:.3}", time);

        let post_lab_score_req = PostLabScoreRequest::new(
            user_id.into(),
            product_data.outline_file.as_str().into(),
            format!("pp6\\{}", url).into(),
            lab_data.task_count as u32,
            lab_data.task_count as u32,
            time,
        );

        println!("Creating Lab Session...");
        let session = self
            .client
            .create_lab_session()
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;
        println!("Created Lab Session: {}", session);

        println!("Opening Lab Session...");
        let _opened = self
            .client
            .post_lab_session_opened(&session)
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;
        println!("Opened Lab Session");

        println!("Getting Lab Status..");
        let lab_status = self
            .client
            .get_lab_session_status(&session)
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;
        println!("Got Lab Status: {:#?}", lab_status);

        println!("Posting Lab Score...");
        let _score = self
            .client
            .post_lab_score(&post_lab_score_req)
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;
        println!("Posted Lab Score");

        println!("Ending Lab Session...");
        let url_data = post_lab_score_req.get_details_percent_encoded();
        let lz_data = lz_string::compress_uri(&url_data)
            .ok_or_else(|| println!("Error: Failed to compress lab details!"))?;
        let _end_session = self
            .client
            .post_lab_session_response(&session, &lz_data)
            .await
            .map_err(|e| println!("Error: {:#?}", e))?;
        println!("Ended Lab Session");

        println!("Getting Lab Status..");
        let lab_status = self.client.get_lab_session_status(&session).await;

        let lab_status = match lab_status {
            Ok(s) => s,
            Err(e) => panic!("Error: {:#?}", e),
        };

        println!("Got Lab Status: {:#?}", lab_status);

        Ok(())
    }
}
