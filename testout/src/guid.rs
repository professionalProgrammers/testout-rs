use rand::{
    prelude::SliceRandom,
    thread_rng,
};
use std::fmt;

pub struct Guid {
    pub value: String,
}

impl Guid {
    pub fn new(value: Option<&str>) -> Option<Self> {
        match value {
            Some(value) => {
                let iter = value
                    .chars()
                    .filter(|c| c.is_alphanumeric()) //Technically, (val[i] >= '0' && val[i] <= '9') || (val[i] >= 'a' && val[i] <= 'f'), but close enough?
                    .flat_map(|c| c.to_lowercase())
                    .take(36 - 4); //Consider a vec,

                let mut value = String::new(); //Collect somehow
                for el in iter {
                    value.push(el);

                    let len = value.len();
                    if len == 8 || len == 13 || len == 18 || len == 23 {
                        value.push('-');
                    }
                }

                if value.len() != 36 {
                    return None;
                }

                Some(Guid { value })
            }
            None => None,
        }
    }

    pub fn from_random(exclude_dashes: bool) -> Self {
        let mut rng = thread_rng();
        let hex = b"0123456789ABCDEF";
        let mut chars = ['0'; 32];
        chars
            .iter_mut()
            .for_each(|c| *c = *hex.choose(&mut rng).unwrap() as char);

        chars[12] = '4';
        chars[16] = hex[(chars[16] as usize & 0x3) | 0x8] as char;

        let formatted_vec = if exclude_dashes {
            chars.to_vec()
        } else {
            let mut vec = Vec::with_capacity(36);
            vec.extend(chars[0..8].iter());
            vec.push('-');
            vec.extend(chars[8..12].iter());
            vec.push('-');
            vec.extend(chars[12..16].iter());
            vec.push('-');
            vec.extend(chars[16..20].iter());
            vec.push('-');
            vec.extend(chars[20..32].iter());
            vec
        };

        let formatted: String = formatted_vec
            .into_iter()
            .flat_map(|c| c.to_lowercase())
            .collect();

        Guid { value: formatted }
    }
}

impl fmt::Display for Guid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.value)
    }
}
