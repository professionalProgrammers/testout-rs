pub mod request;
pub mod response;

use crate::Guid;
use serde::{
    Deserialize,
    Serialize,
};
use std::collections::HashMap;

mod key_value {
    use super::KeyValue;

    use std::collections::HashMap;

    use serde::{
        de::{
            Deserialize,
            Deserializer,
        },
        ser::Serializer,
    };

    pub fn serialize<S>(map: &HashMap<String, String>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let iter = map.iter().map(|(k, v)| KeyValue {
            name: k.to_string(),
            value: v.to_string(),
        });
        serializer.collect_seq(iter)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<HashMap<String, String>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let mut map = HashMap::new();
        for kv in Vec::<KeyValue>::deserialize(deserializer)? {
            map.insert(kv.name, kv.value);
        }
        Ok(map)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Command {
    #[serde(rename = "ID")]
    pub id: String,
    #[serde(rename = "Critical")]
    pub critical: bool,
    #[serde(rename = "CommandType")]
    pub command_type: u32,
    #[serde(rename = "Priority")]
    pub priority: u32,
    #[serde(rename = "Parameters")]
    #[serde(with = "key_value")]
    pub parameters: HashMap<String, String>,
    #[serde(rename = "Status")]
    pub status: u32,
    #[serde(rename = "Objects")]
    pub objects: Vec<Object>,
    #[serde(rename = "SliceIndex")]
    pub slice_index: u32,
    #[serde(rename = "TotalObjectCount")]
    pub total_object_count: u32,
    #[serde(rename = "ObjectsRemaining")]
    pub objects_remaining: u32,
}

impl Command {
    pub fn new(command_type: u32) -> Self {
        Command {
            id: Guid::from_random(false).to_string(),
            critical: false,
            command_type,
            priority: 0,
            parameters: HashMap::new(),
            status: 0,
            objects: Vec::new(),
            slice_index: 0,
            total_object_count: 0,
            objects_remaining: 0,
        }
    }

    pub fn set_param(&mut self, k: String, v: String) {
        self.parameters.insert(k, v);
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CommandList {
    #[serde(rename = "requestPackage")]
    request_package: RequestPackage,
    #[serde(rename = "serviceId")]
    service_id: String,
}

impl CommandList {
    pub fn new(service_id: String, commands: Vec<Command>) -> Self {
        CommandList {
            request_package: RequestPackage {
                commands,
                more_objects_in_queue: false,
            },
            service_id,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Object {
    Object8 {
        #[serde(rename = "ID")]
        id: String,
        #[serde(rename = "Properties")]
        #[serde(with = "key_value")]
        properties: HashMap<String, String>,
    },
    ObjectUnknown {
        #[serde(rename = "ID")]
        id: String,
    },
}

#[derive(Debug, Deserialize, Serialize)]
struct RequestPackage {
    #[serde(rename = "Commands")]
    commands: Vec<Command>,
    #[serde(rename = "MoreObjectsInQueue")]
    more_objects_in_queue: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    #[serde(rename = "ID")]
    pub id: String,
    #[serde(rename = "ObjectType")]
    pub object_type: u32,
    #[serde(rename = "Properties")]
    #[serde(with = "key_value")]
    pub properties: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct KeyValue {
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Value")]
    pub value: String,
}
