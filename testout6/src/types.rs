/// Activated Products and Classes
pub mod activated_products_and_classes;
/// Login types
pub mod login;
/// Outline types
pub mod outline;

pub use self::{
    activated_products_and_classes::{
        ActivatedProduct,
        ActivatedProductsAndClassesResponse,
        AvailableVersion,
        Product as ActivatedProductsAndClassesProduct,
        ProductVersion as ActivatedProductsAndClassesProductVersion,
    },
    login::{
        LoginRequestJson,
        LoginResponse,
    },
    outline::{
        Navigation as OutlineNavigation,
        Outline,
        Resource as OutlineResource,
        Section as OutlineSection,
    },
};
use std::collections::HashMap;
use uuid::Uuid;

/// The type of a resource
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    serde_repr::Serialize_repr,
    serde_repr::Deserialize_repr,
    num_derive::FromPrimitive,
    num_derive::ToPrimitive,
)]
#[repr(u8)]
pub enum ResourceType {
    /// Unknown
    Unknown = 0,

    /// Text
    Text = 1,

    /// Video
    Video = 2,

    /// Sim
    Sim = 3,

    /// Exam
    Exam = 4,

    /// Question
    Question = 5,

    /// Table of Contents
    TableOfContents = 6,

    /// Glossary
    Glossary = 7,

    /// Section
    Section = 8,

    /// Image
    Image = 9,

    /// Certification
    Certification = 10,

    /// Unknown 11
    Unknown11 = 11,

    /// Unknown 12
    Unknown12 = 12,
}

/// An entry in the resource map
#[derive(Debug, Clone, serde::Deserialize)]
pub struct ResourceMapEntry {
    /// ?
    #[serde(rename = "durationSeconds")]
    pub duration_seconds: Option<serde_json::Value>,

    /// ?
    #[serde(rename = "groupId")]
    pub group_id: String,

    /// ?
    #[serde(rename = "pointsPossible")]
    pub points_possible: u32,

    /// The resource id
    #[serde(rename = "resourceId")]
    pub resource_id: Uuid,

    /// The resource index
    #[serde(rename = "resourceIndex")]
    pub resource_index: String,

    /// The resource sub type?
    #[serde(rename = "resourceSubType")]
    pub resource_sub_type: ResourceType,

    /// The resource title
    #[serde(rename = "resourceTitle")]
    pub resource_title: String,

    /// The resource type
    #[serde(rename = "resourceType")]
    pub resource_type: ResourceType,

    /// The resource scoring path
    #[serde(rename = "scoringPath")]
    pub scoring_path: String,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// An exam attempt
#[derive(Debug, serde::Deserialize)]
pub struct Attempt {
    /// ?
    #[serde(rename = "activeClassProfileId")]
    pub active_class_profile_id: Option<serde_json::Value>,

    /// The exam session id
    #[serde(rename = "examSessionId")]
    pub exam_session_id: Option<u32>,

    /// The group id
    #[serde(rename = "groupId")]
    pub group_id: String,

    /// whether the user passed on this attempt
    pub passed: bool,

    /// The number of points possible
    #[serde(rename = "pointsPossible")]
    pub points_possible: u32,

    /// The number of points scored on this attempt
    #[serde(rename = "pointsScored")]
    pub points_scored: f32,

    /*
    profileActionId: null
    resourceId: "b42e3971-ce87-462d-b7c5-936008c02122"
    resourceSubType: 4
    resourceType: 4
    responseDetails: "14117788"
    resultDateTimeUtc: "2021-12-16T03:22:26.437Z"
        */
    /// The result id
    #[serde(rename = "resultId")]
    pub result_id: u32,

    /// The # of seconds spent in exam
    #[serde(rename = "secondsInResource")]
    pub seconds_in_resource: u64,

    /// The user id
    #[serde(rename = "userProfileId")]
    pub user_profile_id: Uuid,

    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// A Tab response
#[derive(Debug, serde::Serialize)]
pub struct TabResponse {
    /// An exam session id
    #[serde(rename = "examSessionId")]
    pub exam_session_id: u64,

    /// The points scored
    pub points: u32,

    /// The number of points possible
    #[serde(rename = "pointsPossible")]
    pub points_possible: u32,

    /// Whether the user passed
    pub passed: bool,

    /// The seconds spent
    #[serde(rename = "totalSeconds")]
    pub total_seconds: u64,

    /// ?
    pub scale_key: Option<serde_json::Value>,

    #[serde(rename = "responseDetails")]
    pub response_details: u64,
}

/// A create exam response
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct CreateExamResponse {
    /// ?
    #[serde(rename = "classProfileId")]
    pub class_profile_id: Option<serde_json::Value>,

    /// ?
    #[serde(rename = "completedDateUtc")]
    pub completed_date_utc: Option<serde_json::Value>,

    /// ?
    #[serde(rename = "createdDateUtc")]
    pub created_date_utc: String,

    /// ?
    #[serde(rename = "customExamResourceId")]
    pub custom_exam_resource_id: Option<serde_json::Value>,

    /// ?
    #[serde(rename = "examFormResourceId")]
    pub exam_form_resource_id: Option<serde_json::Value>,

    /// The exam session id
    #[serde(rename = "examSessionId")]
    pub exam_session_id: u64,

    /*
    isDeleted: false
    isProctored: null
    ls5examSessionGuid: null
    outlineResourceId: "946539dc-6bd9-4438-b350-d1fa93161744"
    passingScore: null
    percentScore: null
    productVersionId: 1187
    profileActionId: null
    scaleKey: null
    scaledScore: null
    totalSeconds: null
    userProfileId: "76e8c62d-6684-4672-9608-ee2b27755d16"
        */
    /// Extra values
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// The request for the submit result call
#[derive(Debug, serde::Serialize)]
pub struct SubmitResultRequest {
    /// The user id
    #[serde(rename = "userProfileId")]
    pub user_profile_id: Uuid,

    /// The resource id
    #[serde(rename = "resourceId")]
    pub resource_id: Uuid,

    /// The resource type
    #[serde(rename = "resourceType")]
    pub resource_type: ResourceType,

    /// The resource sub-type
    #[serde(rename = "resourceSubType")]
    pub resource_sub_type: ResourceType,

    /// ?
    #[serde(rename = "groupId")]
    pub group_id: String,

    /// The number of points scored
    #[serde(rename = "pointsScored")]
    pub points_scored: u8,

    /// The number of points possible
    #[serde(rename = "pointsScored")]
    pub points_possible: u8,

    /// Whether the user passed
    pub passed: bool,

    /// seconds in resource
    #[serde(rename = "secondsInResource")]
    pub seconds_in_resource: u64,

    /// exam id
    #[serde(rename = "responseDetails")]
    pub response_details: u64,

    /// ?
    #[serde(rename = "activeClassProfileId")]
    pub active_class_profile_id: Option<serde_json::Value>,

    /// the exam session id
    #[serde(rename = "examSessionId")]
    pub exam_session_id: u64,
}
