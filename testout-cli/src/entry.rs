use roxmltree::Node;
use std::fmt;

#[derive(Debug, Clone)]
pub enum Entry {
    Section {
        title: String,
        index: String,
        href: String,
        entries: Vec<Entry>,
    },
    Resource {
        title: String,
        index: String,
        href: String,
    },
}

impl Entry {
    pub fn index(&self) -> &str {
        match self {
            Entry::Resource { index, .. } => index,
            Entry::Section { index, .. } => index,
        }
    }

    pub fn href(&self) -> &str {
        match self {
            Entry::Resource { href, .. } => href,
            Entry::Section { href, .. } => href,
        }
    }

    pub fn title(&self) -> &str {
        match self {
            Entry::Resource { title, .. } => title,
            Entry::Section { title, .. } => title,
        }
    }

    pub fn iter_children(&self) -> Option<ChildrenEntryIter> {
        ChildrenEntryIter::new(&self)
    }

    pub fn into_iter_children(self) -> Option<ChildrenEntryIntoIter> {
        ChildrenEntryIntoIter::new(self)
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Entry::Section { index, .. } => write!(f, "Section {}", index),
            Entry::Resource { index, .. } => write!(f, "Resource {}", index),
        }
    }
}

pub struct ChildrenEntryIter<'a> {
    iter: std::slice::Iter<'a, Entry>,
}

impl<'a> ChildrenEntryIter<'a> {
    pub fn new(entry: &'a Entry) -> Option<Self> {
        match entry {
            Entry::Section { entries, .. } => {
                let iter = entries.iter();
                Some(Self { iter })
            }
            _ => None,
        }
    }
}

impl<'a> Iterator for ChildrenEntryIter<'a> {
    type Item = &'a Entry;
    fn next(&mut self) -> Option<&'a Entry> {
        self.iter.next()
    }
}

pub struct ChildrenEntryIntoIter {
    iter: std::vec::IntoIter<Entry>,
}

impl ChildrenEntryIntoIter {
    pub fn new(entry: Entry) -> Option<Self> {
        match entry {
            Entry::Section { entries, .. } => {
                let iter = entries.into_iter();
                Some(Self { iter })
            }
            _ => None,
        }
    }
}

impl Iterator for ChildrenEntryIntoIter {
    type Item = Entry;
    fn next(&mut self) -> Option<Entry> {
        self.iter.next()
    }
}

pub fn extract_exam_data(text: &str) -> Result<Vec<Entry>, String> {
    Ok(roxmltree::Document::parse(text)
        .map_err(|e| format!("{:#?}", e))?
        .root()
        .children()
        .last()
        .ok_or_else(|| String::from("No Nav"))?
        .children()
        .filter(|el| el.tag_name().name() == "Product")
        .last()
        .ok_or_else(|| String::from("No Product"))?
        .children()
        .filter(|el| el.tag_name().name() == "Outline")
        .last()
        .ok_or_else(|| String::from("No Outline"))?
        .children()
        .filter(|el| el.tag_name().name() == "TOC")
        .last()
        .ok_or_else(|| String::from("No TOC"))?
        .children()
        .filter_map(|el| extract_section_or_resource(&el))
        .collect())
}

pub fn extract_section_or_resource(el: &Node) -> Option<Entry> {
    match el.tag_name().name() {
        "Section" => {
            let index = el.attribute("index")?.to_string();
            let title = el.attribute("title")?.to_string();
            let href = el.attribute("href")?.to_string();

            let entries = el
                .children()
                .filter_map(|el| extract_section_or_resource(&el))
                .collect();

            Some(Entry::Section {
                title,
                index,
                href,
                entries,
            })
        }
        "Resource" => {
            let index = el.attribute("index")?.to_string();
            let title = el.attribute("title")?.to_string();
            let href = el.attribute("href")?.to_string();

            Some(Entry::Resource { title, index, href })
        }
        _ => None,
    }
}

pub fn iter_entry_ref<'a>(entry: &'a Entry) -> Box<Iterator<Item = &Entry> + 'a> {
    match entry {
        Entry::Resource { .. } => Box::new(std::iter::once(entry)),
        Entry::Section { entries, .. } => {
            Box::new(std::iter::once(entry).chain(entries.iter().flat_map(|el| iter_entry_ref(el))))
        }
    }
}

pub fn iter_entry(entry: Entry) -> Box<Iterator<Item = Entry>> {
    let entry_cloned = entry.clone();
    match entry {
        Entry::Resource { .. } => Box::new(std::iter::once(entry)),
        Entry::Section { entries, .. } => {
            Box::new(std::iter::once(entry_cloned).chain(entries.into_iter().flat_map(iter_entry)))
        }
    }
}
