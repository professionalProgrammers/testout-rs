use crate::context::Context;
use anyhow::Context as _;
use clap::{
    App,
    Arg,
    ArgMatches,
    SubCommand,
};

pub fn cli() -> App<'static, 'static> {
    SubCommand::with_name("solve")
        .about("Solves Labs and Practice Questions")
        .arg(
            Arg::with_name("username")
                .short("u")
                .long("username")
                .required(true)
                .takes_value(true)
                .help("Testout Username"),
        )
        .arg(
            Arg::with_name("password")
                .short("p")
                .long("password")
                .required(true)
                .takes_value(true)
                .help("Testout Password"),
        )
        .arg(
            Arg::with_name("section")
                .short("s")
                .long("section")
                .takes_value(true)
                .help("Target Section"),
        )
        .arg(
            Arg::with_name("questions")
                .short("q")
                .long("questions")
                .help("Enables targeting for Practice Questions"),
        )
        .arg(
            Arg::with_name("labs")
                .short("l")
                .long("labs")
                .help("Enables Targeting for Labs"),
        )
        .arg(
            Arg::with_name("min-time")
                .long("min-time")
                .help("(WIP) Sets the minimum time in seconds for completion"),
        )
}

pub async fn exec(matches: &ArgMatches<'_>, ctx: &mut Context) -> anyhow::Result<String> {
    let authenticate_response = ctx
        .login(
            matches.value_of("username").context("missing username")?,
            matches.value_of("password").context("missing password")?,
        )
        .await?;
    let user_id = authenticate_response.user.id;

    let min_time = matches
        .value_of("min-time")
        .and_then(|time| time.parse::<u32>().ok())
        .ok_or(|_: u32| println!("Error parsing argument 'min-time'"))
        .unwrap_or(60 * 2);

    Ok(user_id.clone())
}
