use anyhow::{
    ensure,
    Context,
};
use rand::Rng;
use testout6::{
    ResourceType,
    SubmitResultRequest,
};
use uuid::Uuid;

#[derive(argh::FromArgs)]
#[argh(description = "A program to do testout version 6")]
struct Options {
    #[argh(option, short = 'u', long = "username", description = "the username")]
    username: String,

    #[argh(option, short = 'p', long = "password", description = "the password")]
    password: String,

    #[argh(
        switch,
        short = 'd',
        long = "dry-run",
        description = "do not submit any responses"
    )]
    dry_run: bool,

    #[argh(
        option,
        short = 's',
        long = "start-index",
        description = "the index at which to start processing"
    )]
    start_index: Option<String>,

    #[argh(
        option,
        short = 'e',
        long = "end-index",
        description = "the index at which to stop processing"
    )]
    end_index: Option<String>,
}

fn main() {
    let options: Options = argh::from_env();
    let code = match real_main(options) {
        Ok(()) => 0,
        Err(e) => {
            eprintln!("{:?}", e);
            1
        }
    };

    std::process::exit(code);
}

fn real_main(options: Options) -> anyhow::Result<()> {
    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("failed to build tokio runtime")?;

    tokio_rt.block_on(async_main(options))?;

    Ok(())
}

/// The async main function
async fn async_main(options: Options) -> anyhow::Result<()> {
    let client = testout6::Client::new();

    // Log in as the user
    println!("logging in as {}...", options.username);
    println!();
    let login_response = client
        .login(&options.username, &options.password)
        .await
        .context("failed to log in")?;

    // On failure, bail
    ensure!(login_response.success, "login failed");

    // Store the user id from the response.
    // We will need it for other api calls.
    let user_id = login_response
        .authenticated_user
        .as_ref()
        .context("missing user data")?
        .user_profile
        .user_profile_id;
    println!("login response:");
    println!("user id: {}", user_id);
    println!();

    // Locate classes
    println!("locating all classes...");
    println!();
    let classes_response = client
        .get_activated_products_and_classes(user_id)
        .await
        .context("failed to get classes")?;

    // Tell the user about located classes
    println!("located classes:");
    for activated_product in classes_response.activated_products.iter() {
        println!(" * {}", activated_product.product.title);
    }
    println!();

    for activated_product in classes_response.activated_products.iter() {
        let result = complete_product(&client, &options, user_id, activated_product)
            .await
            .context("failed to complete class");
        if let Err(e) = result {
            println!("{:?}", e);
        }
    }

    println!("completed successfully");

    Ok(())
}

/// Complete a "Product" aka a class
async fn complete_product(
    client: &testout6::Client,
    options: &Options,
    user_id: Uuid,
    activated_product: &testout6::ActivatedProduct,
) -> anyhow::Result<()> {
    // Print some product info
    println!("starting class {}...", activated_product.product.title);
    println!("locale: {}", activated_product.product.locale);
    println!(
        "using version id {}...",
        activated_product.default_version_id
    );

    // Select a version?
    let default_version = activated_product
        .get_default_available_version()
        .context("failed to select a version")?;

    // Select a product version?
    let default_product_version = activated_product
        .get_default_product_version()
        .context("failed to select product version")?;

    // Print info about the product version
    println!("selected version:");
    if let Some(class_info) = default_version.class_info.as_ref() {
        for class_info in class_info.iter() {
            println!("    class name: {}", class_info.class_name);
            println!("    teachers:");
            for teacher in class_info.teacher_names.iter() {
                println!("         * {} {}", teacher[0], teacher[1]);
            }
        }
    }
    println!(
        "major version number: {}",
        default_product_version.major_version
    );
    println!(
        "minor version number: {}",
        default_product_version.minor_version
    );
    println!(
        "patch version number: {}",
        default_product_version.patch_version
    );
    println!("outline file: {}", default_product_version.outline_href);

    // Get the outline for the product
    println!("getting outline...");
    let outline = client
        .get_outline(
            default_product_version.major_version,
            default_product_version.minor_version,
            default_product_version.patch_version,
            &activated_product.product.locale,
            &default_product_version.outline_href,
        )
        .await
        .context("failed to get outline")?;

    // Print some outline data
    println!("outline:");
    println!("    title: {}", outline.product_title);
    println!("    sections:");
    for section in outline.product.outline.toc.sections.iter() {
        print_section(section, 2);
    }
    println!();

    // Get the resource map
    println!("getting resource map...");
    let resource_map = client
        .get_resource_map(
            activated_product.default_version_id,
            default_product_version.patch_version,
        )
        .await
        .context("failed to get resource map")?;

    // Iter over exams
    println!("iterating over exams...");
    for maybe_pair in outline
        .product
        .outline
        .toc
        .iter_all_sections()
        .flat_map(|section| section.resources.iter())
        .map(|resource| {
            resource_map
                .get(&resource.href)
                .map(|entry| (resource, entry))
        })
        .skip_while(
            |maybe_pair| match (maybe_pair.as_ref(), options.start_index.as_ref()) {
                (Some((resource, _entry)), Some(start_index)) => {
                    resource.index.as_str() != start_index
                }
                _ => false,
            },
        )
        .take_while(
            |maybe_pair| match (maybe_pair.as_ref(), options.end_index.as_ref()) {
                (Some((resource, _entry)), Some(end_index)) => resource.index.as_str() != end_index,
                _ => true,
            },
        )
        .filter(|maybe_pair| {
            maybe_pair.as_ref().map_or(true, |(_resource, entry)| {
                entry.resource_type == testout6::ResourceType::Exam
                    || entry.resource_type == testout6::ResourceType::Sim
            })
        })
    {
        match maybe_pair {
            Some((resource, entry)) => {
                println!("    starting resource {}", resource.index);
                println!("        getting exam attempts...");
                let exam_attempts_response = client
                    .get_exam_attempts(user_id, entry.resource_id)
                    .await
                    .context("failed to get exam attempts")?;
                let passed = exam_attempts_response.iter().any(|attempt| attempt.passed);
                if passed {
                    println!("        skipping as user has already passed this exam");
                } else {
                    println!("        user has not passed this exam");
                    println!("        creating exam...");
                    if !options.dry_run {
                        let create_exam_response = client
                            .create_exam(
                                user_id,
                                activated_product.default_version_id,
                                entry.resource_id,
                            )
                            .await
                            .context("failed to create exam")?;
                        dbg!(&create_exam_response);
                        println!("        submitting response...");
                        let result_response = client
                            .submit_result(
                                user_id,
                                &SubmitResultRequest {
                                    user_profile_id: user_id,
                                    resource_id: entry.resource_id,
                                    resource_type: ResourceType::Exam,
                                    resource_sub_type: ResourceType::Exam,
                                    group_id: activated_product.default_version_id.to_string(),
                                    points_scored: rand::thread_rng().gen_range(8..10),
                                    points_possible: 10,
                                    passed: true,
                                    seconds_in_resource: rand::thread_rng().gen_range(1..2),
                                    response_details: create_exam_response.exam_session_id,
                                    active_class_profile_id: None,
                                    exam_session_id: create_exam_response.exam_session_id,
                                },
                            )
                            .await
                            .context("failed to submit result")?;
                        dbg!(result_response);
                    } else {
                        println!("        skipping as this is a dry run...");
                    }
                    println!();
                }
            }
            None => {
                println!("warning: resource could not be looked up in resource map");
            }
        }
    }

    Ok(())
}

/// Print a section
fn print_section(section: &testout6::OutlineSection, tab_index: usize) {
    println!(
        "{}section {} ({})",
        "    ".repeat(tab_index),
        section.index,
        section.title
    );

    for resource in section.resources.iter() {
        println!(
            "{}resource {} ({})",
            "    ".repeat(tab_index + 1),
            resource.index,
            resource.title
        );
        println!(
            "{}is exam: {}",
            "    ".repeat(tab_index + 2),
            resource.is_exam()
        );
    }

    for section in section.sections.iter() {
        print_section(section, tab_index + 1);
    }
}
