pub mod client;
mod guid;
pub mod types;

pub use crate::{
    client::Client,
    guid::Guid,
};
use percent_encoding::{
    utf8_percent_encode,
    NON_ALPHANUMERIC,
};
use ratel::ast::{
    expression::Property::Literal as PropertyLiteral,
    statement::DeclarationStatement,
    Expression::{
        Array,
        Object,
    },
    Pattern::Identifier,
    PropertyKey::Literal,
    StatementNode,
};
use ratel_visitor::{
    Visitable,
    Visitor,
};
use serde::Serialize;
use std::borrow::Cow;

/// library error
#[derive(Debug, thiserror::Error)]
pub enum TestoutError {
    /// A Reqwest error
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    #[error("network")]
    Network,

    #[error("invalid body")]
    InvalidBody,

    #[error("invalid api response")]
    InvalidApiResponse,

    #[error("invalid service id")]
    InvalidServiceId,

    #[error("invalid status")]
    InvalidStatus(u16),

    #[error("bad json parse")]
    JsonParse(String, String),

    #[error("bad json build")]
    JsonBuild,

    #[error("bad request construction")]
    RequestConstruction,
}

pub type TestoutResult<T> = Result<T, TestoutError>;

#[derive(Debug, Serialize)]
pub struct PostLabScoreRequest<'a> {
    #[serde(rename = "userGuid")]
    user_id: Cow<'a, str>,
    #[serde(rename = "outlineFile")]
    outline_file: Cow<'a, str>,
    #[serde(rename = "scoringPath")]
    scoring_path: Cow<'a, str>,
    #[serde(rename = "actualScore")]
    actual_score: u32,
    #[serde(rename = "possibleScore")]
    possible_score: u32,
    #[serde(rename = "secondsInResource")]
    seconds_in_resource: u32,
    details: Cow<'a, str>,
}

impl<'a> PostLabScoreRequest<'a> {
    pub fn new(
        user_id: Cow<'a, str>,
        outline_file: Cow<'a, str>,
        scoring_path: Cow<'a, str>,
        actual_score: u32,
        possible_score: u32,
        seconds_in_resource: f32,
    ) -> Self {
        let details = format!(
            "{}¯{}¯{:.3}¯{}¯NaN¯1",
            actual_score,
            possible_score,
            seconds_in_resource,
            "1".repeat(possible_score as usize) //TODO: Outside access/struct for details
        )
        .into(); //"0¯2¯37.379¯0-+0¯NaN¯1"
        PostLabScoreRequest {
            user_id,
            outline_file,
            scoring_path,
            actual_score,
            possible_score,
            seconds_in_resource: seconds_in_resource as u32,
            details,
        }
    }

    pub fn get_submit_str(&self) -> Option<String> {
        let score_data = serde_json::to_string(&self).ok()?;
        lz_string::compress_uri(&score_data)
    }

    pub fn get_details(&self) -> Cow<'a, str> {
        self.details.clone()
    }

    pub fn get_details_percent_encoded(&self) -> String {
        utf8_percent_encode(&self.details, NON_ALPHANUMERIC).to_string()
    }
}

pub struct LabData {
    pub task_count: usize,
}

impl LabData {
    pub fn from_str(data: &str) -> Option<Self> {
        let module = ratel::parse(data).ok()?;
        let mut visitor = LabDataVisitor(None);
        module.visit_with(&mut visitor);
        Some(LabData {
            task_count: visitor.0?,
        })
    }
}

struct LabDataVisitor(Option<usize>);
impl<'ast> Visitor<'ast> for LabDataVisitor {
    fn on_declaration_statement(
        &mut self,
        item: &DeclarationStatement,
        _node: &'ast StatementNode<'ast>,
    ) {
        let first_declarator = match item.declarators.first_element() {
            Some(dec) => dec,
            None => return,
        };

        if let Identifier("simConfig") = **first_declarator.id {
            if let Some(Object(object)) = first_declarator.init.map(|el| **el) {
                for el in object.body {
                    if let PropertyLiteral { key, value } = ***el {
                        if let Literal("tasks") = key.item {
                            if let Array(arr) = **value {
                                self.0 = Some(arr.body.iter().count());
                            }
                        }
                    }
                }
            }
        }
    }
}

pub fn encode_datetime<T: chrono::Timelike + chrono::Datelike>(dt: &T) -> String {
    let milliseconds = 0;
    format!(
        "{:03x}{:01x}{:02x}{:02x}{:02x}{:02x}{:03x}",
        dt.year(),
        dt.month(),
        dt.day(),
        dt.hour(),
        dt.minute(),
        dt.second(),
        milliseconds
    )
}

#[derive(Debug)]
pub struct ProductData {
    pub outline_file: String,
    pub version: String,
    pub language: String,
}

impl ProductData {
    pub fn new(outline_file: String, version: String, language: String) -> Self {
        ProductData {
            outline_file,
            version,
            language,
        }
    }

    pub fn get_version_stripped(&self) -> String {
        self.version.chars().filter(|c| c.is_numeric()).collect()
    }

    pub fn get_file_base(&self) -> Option<&str> {
        self.outline_file.split('.').next()
    }

    pub fn get_url(&self) -> Option<String> {
        let version_stripped = self.get_version_stripped();
        let file_base = self.get_file_base()?;
        Some(format!("https://cdn.testout.com/_version_{version_stripped}/{file_base}-{lang}/{lang}/{file}?v={version}", version_stripped=version_stripped, file_base=file_base, lang=self.language, file=self.outline_file, version=self.version))
    }
}
