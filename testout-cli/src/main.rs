#![feature(unboxed_closures)]

mod commands;
mod context;
mod entry;

use crate::{
    context::Context,
    entry::{
        iter_entry,
        Entry,
    },
};
use chrono::{
    Duration,
    Utc,
};
use clap::{
    App,
    Arg,
    ArgMatches,
    SubCommand,
};
use rand::Rng;
use testout::{
    encode_datetime,
    types::Command,
    ProductData,
};

#[tokio::main]
async fn main() {
    let mut ctx = Context::new();
    let matches = App::new("testout-cli")
        .subcommand(
            SubCommand::with_name("info").arg(Arg::with_name("url").index(1).required(true)),
        )
        .subcommand(commands::solve::cli())
        .get_matches();

    match matches.subcommand() {
        ("info", Some(matches)) => {
            let _url = matches.value_of("url").unwrap(); // TODO: Extract data
            let product_data = ProductData::new(
                String::from("pcpro2018v6.xml"),
                String::from("en-us"),
                String::from("6.0.2"),
            );
            ctx.download_and_parse_outline(&product_data).await.unwrap();
        }
        ("solve", Some(matches)) => {
            let user_id_result = commands::solve::exec(&matches, &mut ctx).await;
            let user_id = match user_id_result {
                Ok(user_id) => user_id,
                Err(e) => {
                    eprintln!("{:?}", e);
                    return;
                }
            };

            solve_exam_questions(&mut ctx, user_id, matches.value_of("section"), matches)
                .await
                .unwrap();
        }
        _ => println!("Invalid Command"),
    }
}

async fn solve_exam_questions(
    ctx: &mut Context,
    user_id: String,
    section: Option<&str>,
    matches: &ArgMatches<'_>,
) -> Result<(), ()> {
    let product_id = ctx.get_product_ids().await?;
    let product_data = ctx.get_product_data(&product_id).await?;

    let raw_entry_data = ctx.download_and_parse_outline(&product_data).await?;
    let entry_data = if let Some(section) = section {
        let entry = raw_entry_data
            .into_iter()
            .find(|el| el.index() == section)
            .ok_or_else(|| println!("Section {} not found", section))?;

        match entry {
            Entry::Section { entries, .. } => {
                entries.into_iter().map(iter_entry).flatten().collect()
            }
            Entry::Resource { .. } => vec![entry],
        }
    } else {
        raw_entry_data
    };

    if matches.is_present("questions") {
        let mut date = Utc::now();
        //Old
        let mut rng = rand::thread_rng();
        for cmd in entry_data
            .iter()
            .filter(|el| el.href().starts_with("resources\\exams"))
            .map(|el| {
                println!("Executing Command 68...");
                let score = rng.gen_range(0.8_f32..1.0_f32);
                let score_string = format!("{:.4}", score);

                let time_taken = Duration::seconds(rng.gen_range(60 * 2..60 * 10));
                date = date + time_taken;

                let mut cmd = Command::new(68);
                cmd.set_param(String::from("userGuid"), user_id.clone());
                cmd.set_param(
                    String::from("outlineFile"),
                    product_data.outline_file.clone(),
                );
                cmd.set_param(String::from("resourceType"), String::from("E")); //e for exam
                cmd.set_param(
                    String::from("resourceName"),
                    String::from("pp6\\") + &el.href(),
                ); //TODO: Why is pp6 necessary?
                cmd.set_param(String::from("resourceTitle"), el.title().to_string());
                cmd.set_param(String::from("sectionIndex"), el.index().to_string());
                cmd.set_param(String::from("requiredScore"), String::from("0.8000"));
                cmd.set_param(String::from("score"), score_string);
                cmd.set_param(
                    String::from("secondsInResource"),
                    time_taken.num_seconds().to_string(),
                ); //resultDateTime
                cmd.set_param(String::from("resultDateTime"), encode_datetime(&date));
                print_command_parameters(&cmd);
                cmd
            })
        {
            let res = ctx.client.execute_commands(vec![cmd]).await.expect("err");
            println!("{:#?}", res);
            println!("Executed Command 68");
        }
        //End Old
    }

    if matches.is_present("labs") {
        for el in entry_data.iter().filter(|el| el.href().starts_with("sims")) {
            match ctx.complete_lab(&product_data, el.href()).await {
                Ok(_) => (),
                Err(_) => println!("Error Completing Lab"),
            }
        }
    }

    if !matches.is_present("questions") && !matches.is_present("labs") {
        println!("No Targets Selected: No action taken");
    }

    Ok(())
}

fn print_command_parameters(cmd: &Command) {
    cmd.parameters.iter().for_each(|(k, v)| {
        println!("Parameter '{}' = '{}'", k, v);
    });
}
